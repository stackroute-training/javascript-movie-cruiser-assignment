let movieData = [];
let favData = [];

function getMovies() {
	return fetch("http://localhost:3000/movies", { method: "GET" })
		.then((res) => {
			if (res.ok) {
				return res.json()
			}
			else {
				return Promise.reject(res.status)
			}
		})
		.then((x) => {
			movieData = x
			displayCollection(movieData)
			return x
		})
		.catch((err) => console.log(err))
}

let displayCollection = (x) => {
	let mainId = document.getElementById("moviesList");
	let htmlString = "";
	x.forEach(element => {
		htmlString += `<li>${element.id}</li>
		              <img src = '${element.posterPAth}' width = 50%>
		              <li>${element.title}</li>		
					  <li><button class='btn btn-primary' onclick='addFavourite(${element.id})'>AddToFavourites</button></li>`

	})
	mainId.innerHTML = htmlString
}

function getFavourites() {
	return fetch("http://localhost:3000/favourites", { method: "GET" })
		.then((res) => {
			if (res.ok) {
				return res.json()
			}
			else {
				return Promise.reject(res.status)
			}
		})
		.then((x) => {
			favData = x
			displayFav(x)
			return x
		})
		.catch(err => console.log(err))
}

function addFavourite(id) {
	let movie = movieData.find(x => {
		if (x.id == id) {
			return x
		}
	})

	let fav = favData.find(x => {
		if (x.id == movie.id) {
			return x
		}
	})

	if(fav) {
        return Promise.reject(new Error('Movie is already added to favourites'));
    }else{
		return fetch(`http://localhost:3000/favourites`,{
        method: 'POST',
        headers: {
            'content-type': 'application/json'
        },
        body: JSON.stringify(movie)
		}
		).then(res => {
				if(res.ok){
					return res.json();
				}
			}
		).then(x => {
				favData.push(x);
				displayFav(favData);
				return favData;
			}
		)
	}
}

let displayFav = (x) => {
	let mainId = document.getElementById("favouritesList");
	let htmlString = "";
	x.forEach(element => {
		htmlString += `<li>${element.id}</li>
		              <img src = '${element.posterPAth}' width = 50%>
		              <li>${element.title}</li>		
					  `
	});
	mainId.innerHTML = htmlString

}

module.exports = {
	getMovies,
	getFavourites,
	addFavourite
};

// You will get error - Uncaught ReferenceError: module is not defined
// while running this script on browser which you shall ignore
// as this is required for testing purposes and shall not hinder
// it's normal execution


